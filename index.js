const express = require('express');
const app = express();
const server = require('http').Server(app);
const path = require('path')
app.use('/static',express.static('public'))
app.get('/**',(req,res)=>{
    return res.sendFile(path.join(__dirname+'.public/index.html'))
})
const io= require('socket.io')(server,{
    
    cors: { origin: '*',
    methods: ["GET", "POST"],
    credentials: true
},
});
// const { ExpressPeerServer } = require('peer');
// const peerServer = ExpressPeerServer(server, {
//     debug: true,
//     });
// app.use('/peerjs', peerServer);


io.on('connection',(socket)=>{
    socket.on('join-room',(roomId,userId)=>{
        socket.join(roomId)
        console.log('usersid',userId,roomId)
        io.to(roomId).emit('user-connected',userId)
    socket.on('disconnect',()=>{
        io.to(roomId).emit('user-disconnected',userId)

    })
    })
})
server.listen(4000)